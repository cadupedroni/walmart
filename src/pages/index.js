import React, { Component } from 'react'
import styled from 'styled-components'
import Button from './../components/button'
import Header from './../components/header'

import ThumbChuveiro from './../images/thumb-chuveiro.jpg'
import ThumbPneu from './../images/thumb-pneu.jpg'
import ThumbBarbeador from './../images/thumb-barbeador.jpg'

const Container = styled.div`
  margin: 30px 120px;
  overflow-x: none;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  @media (max-width: 768px) {
    width: 100%;
    margin: 0;
  }
`
const ListItens = styled.div`
  margin: 0;
  padding: 0;
`
const Section = styled.div`
  background: #ffffff;
  margin: 0 auto 30px auto;
  border-bottom: 3px solid #3a7491;
  border-radius: 4px 4px 1px 1px;
  display: flex;
`
const Image = styled.div`
  text-align: center;
  flex: 2;
`
const Content = styled.div`
  margin-top: 15px;
  flex: 8;
  @media (max-width: 768px) {
    flex: 4;
  }
`
const Name = styled.h2`
  font-size: 28px;
  color: #666;
  line-height: 28px;
  font-weight: 400;
  margin-bottom: 1rem;
  @media (max-width: 768px) {
    font-size: 18px;
    padding-right: 5px;
  }
`
const Description = styled.p`
  font-size: 18px;
  color: #666666;
  padding-right: 50px;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const Img = styled.img`
  margin: 20px 0;
`
const Row = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`

/* Database with JSON - Orders */
export const orders = [
  {
    id: 1,
    name: 'Ducha Hydra-Corona Digital Temperatura Gradual Polo Plus',
    description: 'A Ducha Digital Polo Plus, da Hydra-Corona, possui sistema eletrônico gerenciado por haste que, junto do visor digital, controla de forma gradual a temperatura da água. Tem, ainda, espalhador com formato retangular para levar a água de ombro a ombro. O produto dispõe de 7700W de potência para 220V e 5500W para 110V.',
    image: ThumbChuveiro
  },
  {
    id: 2,
    name: 'Kit com 4 Pneus Aro 15 Goodyear 195/55R15 85H Direction Sport',
    description: 'Desenvolvido para o uso diário, o pneu Goodyer Direction Sport é a escolha ideal para quem busca performance tanto nos percursos dentro da cidade como para pegar a estrada e se aventurar por aí.',
    image: ThumbPneu
  },
  {
    id: 3,
    name: 'Barbeador Elétrico Philips Aquatouch AT891/14 Recarregável Bivolt',
    description: 'O barbeador elétrico AquaTouch AT891 se adapta às curvas do rosto, cortando até os pelos mais curtos e proporcionando para os homens um barbear com menos atrito entre o aparelho e a pele.',
    image: ThumbBarbeador
  }
]
export default class Orders extends Component {
  render() {
    return (
      <div>
        <Header siteTitle="Pedidos" />
        <Container>
          {orders.map(order => {
            return(
              <ListItens key={order.id}>
                <Section>
                  <Image>
                    <Img src={order.image} alt={order.name} />
                  </Image>
                  <Content>
                    <Name>{order.name}</Name>
                    <Description>{order.description}</Description>                  
                  </Content>
                </Section>
              </ListItens>
            )
          })}
          <Row>
            <Button href="/address" label="Endereços" />
            <Button href="/profile" label="Perfil" />  
          </Row>
        </Container>
      </div>
    )
  }
}