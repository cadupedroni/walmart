import React, { Component } from 'react'
import styled from 'styled-components'
import Button from './../components/button'
import Header from './../components/header'

const Container = styled.div`
  margin: 30px 120px;
  overflow-x: none;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  @media (max-width: 768px) {
    width: 100%;
    margin: 0;
  }
`
const ListItens = styled.div`
  margin: 0;
  padding: 0;
`
const Section = styled.div`
  background: #ffffff;
  margin: 0 auto 30px auto;
  padding: 20px;
  border-bottom: 3px solid #3a7491;
  border-radius: 4px 4px 1px 1px;
  display: flex;
`
const Content = styled.div`
  margin-top: 15px;
  flex: 8;
  @media (max-width: 768px) {
    flex: 4;
  }
`
const Name = styled.h2`
  font-size: 18px;
  color: #999;
  font-weight: normal;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const CPF = styled.p`
  font-size: 18px;
  color: #999;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const Gender = styled.p`
  font-size: 18px;
  color: #999;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const BirthDate = styled.p`
  font-size: 18px;
  color: #999;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const Telephone = styled.p`
  font-size: 18px;
  color: #999;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const Email = styled.p`
  font-size: 18px;
  color: #999;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const Label = styled.p`
  font-size: 18px;
  color: #666;
  margin-bottom: 0;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const Row = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`

/* Database with JSON - Profiles */
export const profiles = [
  {
    id: 1,
    name: 'Nome completo do cliente',
    cpf: '123.***.***-**',
    gender: 'Masculino',
    birth_date: '31/10/1981',
    telephone: '(11) 92222-1234',
    email: 'email@email.com'
  }
]

export default class Profiles extends Component {
  render() {
    return (
      <div>
        <Header siteTitle="Dados pessoais" />
        <Container>
          {profiles.map(profile => {
            return(
              <ListItens key={profile.id}>
                <Section>
                  <Content className="flex-item-1">
                    <Label>Nome</Label>
                    <Name>{profile.name}</Name>
                    <Label>CPF</Label>
                    <CPF>{profile.cpf}</CPF>
                    <Label>Sexo</Label>
                    <Gender>{profile.gender}</Gender>
                  </Content>
                  <Content className="flex-item-1">
                    <Label>Data de Nascimento</Label>
                    <BirthDate>{profile.birth_date}</BirthDate>
                    <Label>Telefone</Label>
                    <Telephone>{profile.telephone}</Telephone>
                    <Label>E-mail</Label>
                    <Email>{profile.email}</Email>
                  </Content>
                </Section>
              </ListItens>
            )
          })}
          <Row>
            <Button href="/" label="Pedidos" />
            <Button href="/address" label="Endereços" />
          </Row>
        </Container>
      </div>
    )
  }
}