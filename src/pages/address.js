import React, { Component } from 'react'
import styled from 'styled-components'
import Button from './../components/button'
import Header from './../components/header'

const Container = styled.div`
  margin: 30px 120px;
  overflow-x: none;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  @media (max-width: 768px) {
    width: 100%;
    margin: 0;
  }
`
const ListItens = styled.div`
  margin: 0;
  padding: 0;
`
const Section = styled.div`
  background: #ffffff;
  margin: 0 auto 30px auto;
  padding: 20px;
  border-bottom: 3px solid #3a7491;
  border-radius: 4px 4px 1px 1px;
  display: flex;
`
const Content = styled.div`
  margin-top: 15px;
  flex: 8;
  @media (max-width: 768px) {
    flex: 4;
  }
`
const Name = styled.h2`
  font-size: 28px;
  color: #666;
  line-height: 28px;
  font-weight: 400;
  margin-bottom: 1rem;
  @media (max-width: 768px) {
    font-size: 18px;
    padding-right: 5px;
  }
`
const Address = styled.p`
  font-size: 18px;
  color: #666;
  margin-right: 50px;
  margin-bottom: 0;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const Location = styled.p`
  font-size: 18px;
  color: #666;
  margin-right: 50px;
  margin-bottom: 0;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const CEP = styled.p`
  font-size: 18px;
  color: #666;
  margin-right: 50px;
  margin-bottom: 0;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`
const Row = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`

/* Database with JSON - Address */
export const addresses = [
  {
    id: 1,
    name: 'Apartamento',
    address: 'Rua Exemplo, 001 - Bloco 01 - Apartamento 01',
    location: 'Paulista - São Paulo - SP',
    cep: 'CEP 11111-111'
  },
  {
    id: 2,
    name: 'Trabalho',
    address: 'Rua Exemplo, 002',
    location: 'Itaim Bibi - São Paulo - SP',
    cep: 'CEP 222222-222'
  },
  {
    id: 3,
    name: 'Mãe',
    address: 'Rua Exemplo, 003',
    location: 'Pinheiros - São Paulo - SP',
    cep: 'CEP 333333-333'
  }
]

export default class Addresses extends Component {
  render() {
    return (
      <div>
        <Header siteTitle="Endereços" />
        <Container>
          {addresses.map(address => {
            return(
              <ListItens key={address.id}>
                <Section>
                  <Content className="flex-item-1">
                    <Name>{address.name}</Name>
                    <Address>{address.address}</Address>
                    <Location>{address.location}</Location>
                    <CEP>{address.cep}</CEP>
                  </Content>
                </Section>
              </ListItens>
            )
          })}
          <Row>
            <Button href="/" label="Pedidos" />
            <Button href="/profile" label="Perfil" />        
          </Row>
        </Container>
      </div>
    )
  }
}