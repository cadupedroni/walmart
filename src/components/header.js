import React, { Component } from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'

const Container = styled.div`
  background: #1a75ce;
  margin-bottom: 1.45rem;
  @media (max-width: 768px) {
    width: 100%;
    margin: 0;
  }
`
const Alignment = styled.div`
  margin: 0 auto;
  max-width: 960px;
  padding: 1.45rem 0;
`
const Title = styled.h1`
  margin: 0;
  @media (max-width: 768px) {
    margin: 0 15px;
  }
`
const TitleLink = styled(Link)`
  color: #fff;
  text-decoration: none;
`
export default class Header extends Component {
  render() {
    return (
      <Container>
        <Alignment>
          <Title>
            <TitleLink to="/">
              {this.props.siteTitle}
            </TitleLink>
          </Title>
        </Alignment>
      </Container>
    )
  }
}