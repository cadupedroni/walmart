import React, {Component} from 'react'
import styled from 'styled-components'
import { Link } from "gatsby"

const Item = styled.div`
  flex: 1;
  flex-grow: 0;
`

const ButtonLink = styled(Link)`
  width: auto;
  font-size: 16px;
  color: #ffffff;
  margin: 15px;
  padding: 10px 80px;
  text-align: center;
  cursor: pointer;
  text-decoration: none;
  background: #1e8adf;
  border: 1px solid #ffffff;
  border-radius: 30px;
  display: block;
`

export default class Button extends Component{
  render() {
    switch(this.props.type) {
      default: 
      return(
        <Item>
          <ButtonLink to={this.props.href}>{this.props.label}</ButtonLink>
        </Item>
      )
    }
  }
}