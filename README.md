<h1 align="center">
  Walmart - Test Front-End
</h1>

## Why React?

Is a library in javascript very fast and complete capabilities for applications that have changing data all the time, so it was designed to create the interfaces of Facebook and Instagram.

The choice of GatsbyJS is that allows to choose different templates for use in various cases since, blog with cms, using template with material design, portfolio, wordpress, with bootstrap among other options.

Follow some of the main features offered:

## Performance

* Static Content
* CDN
* Progressive Web Application (PWA)
* Offline access
* Prefetch linked pages
* Page caching
* Faster time to interaction
* Progressive image loading
* Responsive image loading
* Inlines critical CSS
* Font self-hosting

## Developer Experience

* Serverless
* Export the Code
* Faster edit/debug cycle time
* Refresh or link to preview
* Hot Reload Code
* Declarative frameworks
* Componentization
* One* way data binding
* Declarative data API queries (GraphQL)
* Declarative use
* Modern code syntax
* Asset pipelines
* CSS Extensions
* Advanced JavaScript

## Styled Components (CSS)

* <b>Automatic critical CSS:</b> styled-components keeps track of which components are rendered on a page and injects their styles and nothing else, fully automatically. Combined with code          splitting, this means your users load the least amount of code necessary.
* <b>No class name bugs:</b> styled-components generates unique class names for your styles. You never have to worry about duplication, overlap or misspellings.
* <b>Easier deletion of CSS:</b> it can be hard to know whether a class name is used somewhere in your codebase. styled-components makes it obvious, as every bit of styling is tied to a specific    component. If the component is unused (which tooling can detect) and gets deleted, all its styles get deleted with it.
* <b>Simple dynamic styling:</b> adapting the styling of a component based on its props or a global theme is simple and intuitive without having to manually manage dozens of classes.
* <b>Painless maintenance:</b> you never have to hunt across different files to find the styling affecting your component, so maintenance is a piece of cake no matter how big your codebase is.
* <b>Automatic vendor prefixing:</b> write your CSS to the current standard and let styled-components handle the rest. You get all of these benefits while still writing the CSS you know and love,   just bound to individual components.


## 🚀 Quick start

1.  **Clone Project.**

    Visit the Gitlab website to check the project repository and to install follow the steps below:

    ```sh
    git clone git@gitlab.com:cadupedroni/walmart.git
    ```

2.  **Install Gatsby.**
    
    ```sh
    npm install --global gatsby-cli
    ```

3.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```sh
    cd walmart/
    yarn install
    gatsby develop
    ```

4.  **Open the source code and start editing!**

    ```sh
    Your site is now running at `http://localhost:8000`!
    ```

5. **Test**

   The unit tests are within the __tests ____ folder, at first we have tests to check the rendering of the button, header, and image component.

   To activate the test you only need to run the command below in the root of the project.

   ```sh
   yarn test
   ```

6. **Deploy**

   To make a production package follow the steps below to generate the public directory that contains all the files for the public version of the application.

   ```sh
   cd walmart/
   gatsby build
   Copy the contents of the public/ folder and move to the root of the production server
   ```
    
## 🧐 What's inside?

A quick look at the top-level files and directories you'll see in a Gatsby project.

    .
    ├── node_modules
    ├── src
    ├── .gitignore
    ├── .prettierrc
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    ├── README.md
    └── yarn.lock

  1.  **`/node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages) are automatically installed.  
  
  2.  **`/src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. `src` is a convention for “source code”.
  
  3.  **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.
  
  4.  **`.prettierrc`**: This is a configuration file for [Prettier](https://prettier.io/). Prettier is a tool to help keep the formatting of your code consistent.
  
  5.  **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage of the [Gatsby browser APIs](https://www.gatsbyjs.org/docs/browser-apis/) (if any). These allow customization/extension of default Gatsby settings affecting the browser.
  
  6.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more detail).
  
  7.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.
  
  8.  **`gatsby-ssr.js`**: This file is where Gatsby expects to find any usage of the [Gatsby server-side rendering APIs](https://www.gatsbyjs.org/docs/ssr-apis/) (if any). These allow customization of default Gatsby settings affecting server-side rendering.
  
  9.  **`LICENSE`**: Gatsby is licensed under the MIT license.
  
  10.  **`package-lock.json`** (See `package.json` below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(You won’t change this file directly).**
  
  11.  **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.
  
  12.  **`README.md`**: A text file containing useful reference information about your project.
  
  13.  **`yarn.lock`**: [Yarn](https://yarnpkg.com/) is a package manager alternative to npm. You can use either yarn or npm, though all of the Gatsby docs reference npm.  This file serves essentially the same purpose as `package-lock.json`, just for a different package management system.